# Xnumber

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://vyush.gitlab.io/Xnumber.jl/dev)
[![Build Status](https://gitlab.com/vyush/Xnumber.jl/badges/master/pipeline.svg)](https://gitlab.com/vyush/Xnumber.jl/pipelines)
[![Coverage](https://gitlab.com/vyush/Xnumber.jl/badges/master/coverage.svg)](https://gitlab.com/vyush/Xnumber.jl/commits/master)

This package `Xnumber.jl` that contains x-number structure and its arithmetic implemented as functions. This package contains functions to initialize X-number, normalize X-number, Convert it to floating point number, functions to do arithmetic operations.

## Installing Package

You can obtain X-Number using Julia's Pkg REPL-mode (hitting `]` as the first character of the command prompt):
```julia
(@v1.6) pkg> add "https://gitlab.com/vyush/Xnumber.jl.git"
```
or with
```julia
julia> using Pkg;
julia> Pkg.add(url = "https://gitlab.com/vyush/Xnumber.jl.git")
```
## Importing Package Tools
After installing the package you need to:
```julia
julia> using Xnumber
```

Now you are ready to use the tools provided by package.
