push!(LOAD_PATH,"..")
using Xnumber
using Documenter

DocMeta.setdocmeta!(Xnumber, :DocTestSetup, :(using Xnumber); recursive=true)

makedocs(;
    modules=[Xnumber],
    authors="Vyush Agarwal",
    repo="https://gitlab.com/vyush/Xnumber.jl/blob/{commit}{path}#{line}",
    sitename="Xnumber.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://vyush.gitlab.io/Xnumber.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
deploydocs(
    repo = "gitlab.com/vyush/Xnumber.jl.git",
)
