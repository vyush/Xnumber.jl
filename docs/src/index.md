```@meta
CurrentModule = Xnumber
DocTestSetup = :(using Xnumber)
```

# X-Number

Documentation for [Xnumber](https://gitlab.com/vyush/Xnumber.jl).

This package `Xnumber.jl` that contains x-number structure and its arithmetic implemented as functions. This package contains functions to initialize X-number, normalize X-number, Convert it to floating point number, functions to do arithmetic operations.

```jldoctest
julia> x_init(2,3)
xnumber(2.0, 3)

julia> x_norm(x_init(32.3121231515563e124,3))
xnumber(2.4099482421280953e-29, 4)

julia> x_x2dbl(xnumber(2e-153,1))
26.815615859885195

julia> x_mult(3,xnumber(1e-10,3))
xnumber(3.0e-10, 3)
```
## Installing Package

You can obtain Xnumber using Julia's Pkg REPL-mode (hitting `]` as the first character of the command prompt):
```julia
(@v1.6) pkg> add "https://gitlab.com/vyush/Xnumber.jl.git"
```
or with
```julia
julia> using Pkg;
julia> Pkg.add(url = "https://gitlab.com/vyush/Xnumber.jl.git")
```
## Importing Package Tools
After installing the package you need to:
```julia
julia> using Xnumber
```

Now you are ready to use the tools provided by package.

```@index
Xnumber.xnumber
Xnumber.x_init
Xnumber.x_x2dbl
Xnumber.x_norm
Xnumber.x_sub
Xnumber.x_mult
Xnumber.x_multadd
```

```@docs
Xnumber.xnumber
Xnumber.x_init
Xnumber.x_x2dbl
Xnumber.x_norm
Xnumber.x_sub
Xnumber.x_mult
Xnumber.x_multadd
```
