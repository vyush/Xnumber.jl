using Xnumber
"""
    x_x2dbl(X::xnumber)::Float64

`X_X2DBL` converts the xnumber to a floating point number.
# Input:
- `X:xnumber`: A xnumber.
# Output:
- `Z::Float`: A floating point number corresponding to the given x-number.
# Example:
```julia-repl
julia> x_x2dbl(xnumber(2e-153,1))
26.815615859885195

julia> x_x2dbl(xnumber(2.35,0))
2.35
```
"""
function x_x2dbl(X::xnumber)::Float64

  if ((X.i) == 0)
    return X.x;
  else
    return X.x* (2.0^(X.i * (@BIG)));
  end
end
