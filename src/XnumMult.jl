using Xnumber
"""
    x_mult(alpha::Number, X::xnumber)::xnumber

`X_MULT` multiplication of xnumbers with a scalar: Z =  alpha * X.

# Input:
- `alpha::Number`: A scalar value.
- `X::xnumber`: A number in xnumber format.

# Output:
- `Z::xnumber`: The xnumber for alpha * X .

# Example:
```julia-repl
julia> x_mult(3,xnumber(1e-10,3))
xnumber(3.0e-10, 3)
```
"""
function x_mult(alpha::Number, X::xnumber)::xnumber
  alpha = float(alpha)
  Z = xnumber(0.0,0);
  Z.x = alpha * X.x;
  Z.i = X.i;
  return Z;
end
