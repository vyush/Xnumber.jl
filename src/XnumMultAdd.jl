using Xnumber
"""
    x_multadd(alpha::Number,X::xnumber,beta::Number,Y::xnumber)::xnumber

`X_MULTADD` factor multiplication xnumbers addition: Z = alpha * X + beta * Y

# Input:
- `alpha::Number`: A scalar value.
- `beta::Number`: A scalar value.
- `X::xnumber`: A number in xnumber format.
- `Y::xnumber`: A number in xnumber format.
# Output:
- `Z::xnumber`: The xnumber for alpha * X + beta * Y.
# Example:
```julia-repl
julia> x_multadd(2,xnumber(1,2),3,xnumber(1,1))
xnumber(2.0, 2)

julia> x_multadd(2,xnumber(1,1),3,xnumber(1,1))
xnumber(5.0, 1)

julia> x_multadd(2,xnumber(1,1),3,xnumber(1,2))
xnumber(2.6815615859885194e154, 2)
```
"""
function x_multadd(alpha::Number,X::xnumber,beta::Number,Y::xnumber)::xnumber
    alpha = float(alpha);
    # print(typeof(alpha))
    beta = float(beta);
    Z = xnumber(0.0,0);
    Z.x = 1;
    # print(Z)
    id = X.i - Y.i;
    if (id == 0)
        Z = xnumber((alpha * X.x) + (beta * Y.x),X.i);
    elseif (id >= 1)
        Z = xnumber(alpha * X.x + beta * Y.x * (2.0 ^(-id * (@BIG))),X.i);
    else

        Z = xnumber(alpha * X.x * (2.0 ^(-id * (@BIG))) + beta * Y.x,Y.i);
    end

end
