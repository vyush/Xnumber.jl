using Xnumber
"""
    x_norm(X::xnumber)::xnumber

`X_NORM` normalises an xnumber

# Input:
- `X::xnumber`: A xnumber.
# Output:
- `Z::xnumber`: The normalized xnumber.
# Example:
```julia-repl
julia> x_norm(x_init(32.3121231515563e124,3))
xnumber(2.4099482421280953e-29, 4)

julia> x_norm(x_init(32.3121231515563e-260,3))
xnumber(5.808728196237846e49, 1)
```
"""
function x_norm(X::xnumber)::xnumber
    Z = X;
    r = 0.0;
    ex = 0;
    while(true)
        (r,ex) = frexp(Z.x);
        # print(ex);
        if (ex >= (@BIGS))
            Z.x = float(r) * (2.0^(ex-(@BIG)));
            Z.i = Z.i + 1;
        elseif (ex < -(@BIGS))
            Z.x = float(r) * (2.0^(ex+(@BIG)));
            Z.i = Z.i - 1;
        else
            break;
        end
    end
    return Z;
end
