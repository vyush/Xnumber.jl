using Xnumber
"""
    x_sub(X::xnumber , Y::xnumber)::xnumber

`X_SUB` x-numbers subtraction: Z = X - Y


# Input:
- `X::xnumber`: A xnumber.
- `Y::xnumber`: A xnumber.
# Output:
- `Z::xnumber`: The xnumber for Z = X - Y.
# Example:
```julia-repl
julia> x_sub(xnumber(1,2) ,  xnumber(2,1))
xnumber(1.0, 2)

julia> x_sub(xnumber(2,1) ,  xnumber(1,0))
xnumber(1.0, 2)

julia> x_sub(xnumber(1,1) ,  xnumber(2,1))
xnumber(1.0, 2)
```
"""
function x_sub(X::xnumber , Y::xnumber)::xnumber
    Z = xnumber(0.0,0);
    id = X.i - Y.i;
    if (id == 0)
        Z.x = X.x - Y.x;
        Z.i = X.i;
    elseif (id>=1)
        Z.x = X.x - (Y.x * 2.0^(-id * (@BIG)));
        Z.i = X.i;
    else
        Z.x = (X.x * 2.0^ (-id * (@BIG))) - Y.x;
        Z.i = Y.i;
    end
    return Z;
end
