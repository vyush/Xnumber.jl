module Xnumber

include("xnumberstructure.jl")

export @BIG,@BIGS,xnumber

include("xnumInit.jl")

export x_init

include("XnumSubtact.jl")

export x_sub

include("XnumMult.jl")

export x_mult

include("XnumMultAdd.jl")

export x_multadd

include("Xnum2Dec.jl")

export x_x2dbl

include("XnumNorm.jl")

export x_norm

end
