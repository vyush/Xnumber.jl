using Xnumber
"""
    x_init(x::Number,i::Int)::xnumber

`X_INIT` initalizes xumber to xnumber(x::Float,i::Int) where the xnumber is x*B^i.

# Input:
- `x::Number`: The significand.
- `i::Int`:The exponent.

# Output:
- `Z::xnumber`:The (x,i) as a xnumber struct.

# Example:
```julia-repl
julia> x_init(2,3)
xnumber(2.0, 3)
```
"""
function x_init(x::Number,i::Int)::xnumber
    x = float(x)
    Z = xnumber(x,i);
    return Z;
end
