using Xnumber
macro BIG()
    return :(512)
end
macro BIGS()
    return:((@BIG)/2)
end
"""
    xnumber(x::Float64,i::Int)

`XNUMBER` creates a julia structure with the significand and the exponent.
of the form x*B^i where B is 2^512.
"""
mutable struct xnumber
    x::Float64
    i::Int
end
