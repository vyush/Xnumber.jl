using Xnumber
using Test

@testset "Xnumber.jl" begin
    @test x_x2dbl(xnumber(2e-153,1)) == 26.815615859885195
    @test (x_init(2,3).x == xnumber(2.0, 3).x && x_init(2,3).i == xnumber(2.0, 3).i)
    @test (x_mult(3,xnumber(1e-10,3)).x == xnumber(3.0e-10, 3).x && x_mult(3,xnumber(1e-10,3)).i == xnumber(3.0e-10, 3).i)
    @test (x_norm(x_init(32.3121231515563e124,3)).x == x_init(2.4099482421280953e-29, 4).x && x_norm(x_init(32.3121231515563e124,3)).i == x_init(2.4099482421280953e-29, 4).i)
    @test (x_multadd(2,xnumber(1,2),3,xnumber(1,1)).x == xnumber(2.0, 2).x && x_multadd(2,xnumber(1,2),3,xnumber(1,1)).i == xnumber(2.0, 2).i)
    @test (x_norm(x_init(32.3121231515563e124,3)).x == xnumber(2.4099482421280953e-29, 4).x && x_norm(x_init(32.3121231515563e124,3)).i == xnumber(2.4099482421280953e-29, 4).i)
    @test (x_sub(xnumber(1,2) ,  xnumber(2,1)).x == xnumber(1.0, 2).x && x_sub(xnumber(1,2) ,  xnumber(2,1)).i == xnumber(1.0, 2).i)
    @test (xnumber(1.0,0).x == xnumber(1.0,0).x)
    @test (@BIG) == 512
    @test (@BIGS) == 256
    @test (x_sub(xnumber(2,1) ,  xnumber(1,0)).x == xnumber(2.0, 1).x && x_sub(xnumber(2,1) ,  xnumber(1,0)).i == xnumber(2.0, 1).i)
    @test (x_sub(xnumber(1,1) ,  xnumber(2,1)).x == xnumber(-1.0, 1).x &&  x_sub(xnumber(1,1) ,  xnumber(2,1)).i == xnumber(-1.0, 1).i)
    @test (x_norm(x_init(32.3121231515563e-260,3)).x == xnumber(5.808728196237846e49, 1).x && x_norm(x_init(32.3121231515563e-260,3)).i == xnumber(5.808728196237846e49, 1).i)
    @test (x_multadd(2,xnumber(1,1),3,xnumber(1,1)).x == xnumber(5.0, 1).x && x_multadd(2,xnumber(1,1),3,xnumber(1,1)).i == xnumber(5.0, 1).i)
    @test (x_multadd(2,xnumber(1,1),3,xnumber(1,2)).x == xnumber(2.6815615859885194e154, 2).x && x_multadd(2,xnumber(1,1),3,xnumber(1,2)).i == xnumber(2.6815615859885194e154, 2).i)
    @test (x_x2dbl(xnumber(2.35,0)) == 2.35)
end
